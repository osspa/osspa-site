

#FROM registry.access.redhat.com/ubi9/nodejs-20:1-59.1725850623

FROM registry.access.redhat.com/ubi9/nodejs-20:1-1736309818

WORKDIR  /usr/src/publicwebsite

USER root

RUN useradd paweb

RUN chown -R 1002:1002 /opt/app-root/ /usr/src/

#RUN chown -R 1003420000:0 /opt/app-root/ /usr/src


USER paweb

RUN npm install -g npm@9.8.0

COPY package*.json ./

RUN npm install --legacy-peer-deps --force

#RUN npm audit fix

#RUN npm ci --production && npm prune --production




#RUN apt update && apt -y upgrade

#RUN npm i --package-lock-only --force

#RUN npm audit fix --force

COPY . .

# Build frontend JS assets
RUN npm run build 

USER root

RUN chown -R 1002:1002 /opt/app-root/src/.npm

#clean up unwanted package after build 

RUN chmod 777 -R ./asset/architect/portfolio/tool

USER paweb


EXPOSE 8080

CMD [ "npm", "run" ,"start:production" ]
