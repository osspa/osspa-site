export const demoContent = [
    {
      section: "AI Platform",
      demos: [

        {
          title: "Red Hat Developer Hub is also for Data Scientists",
          description: "Unlock AI/ML tools tailored for data science innovation.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/xCUmJinfCCOziTiRh4Rt?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "How to Download & Setup InstructLab",
          description: "Quick steps to get started with InstructLab.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/5kXP6BZ7HumkupQ89SC1?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Train LLMs with synthetic data in InstructLab",
          description: "See how to train models using synthetic data.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/eTE6P4AjFyVIeRxNZX1C?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Creating a Chatbot with AI in Developer Hub",
          description: "Learn how to build a chatbot using AI tools.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/F5cl0T1PY3p2eOxs0XnZ?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Hands-On with InstructLab [Full Overview]",
          description: "An in-depth look at InstructLab’s capabilities.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/v6gelYGKnrLcn9ZurAhS?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Hosting Your Own Private & Local AI Model with InstructLab",
          description: "Discover how to deploy AI models privately.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/hsViHFIgLO3MPBbWD1wi?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "GPU Enablement on Single Node OpenShift",
          description: "Enable GPUs for AI workloads on OpenShift nodes.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/t95FlUL0InmvrRWjF0Pf?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "AI/ML Workloads at the Edge with MicroShift Image Mode",
          description: "Run AI/ML efficiently on edge devices with MicroShift.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/McC1THUEDxwS5gm1JFly?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "AI/ML Lifecycle Automation at the Edge",
          description: "Automate AI/ML model updates and deployment at the edge.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/cWF2I2oWP0bqkkE2Tr1f?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "RHEL AI - Download LLM",
          description: "Learn to access and deploy large language models.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/X1GGggRtLE6BkIXMT8sB?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "RHEL-AI-LLM-Chat",
          description: "Explore chat capabilities powered by large language models.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/6IiCvpJPKfp5BIQBNU5l?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Using PDF Documents for Model Fine-Tuning with InstructLab & Docling",
          description: "Fine-tune models using PDF documents with InstructLab and Docling.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/H6dvLJ3YfalRoFxF7tiB?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Using PDF Documents for Model Fine-Tuning with InstructLab & Docling" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "OpenShift AI - Creating a Workbench",
          description: "Create a workbench environment with OpenShift AI.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/HCM90gE6WSfRnjhCdQdy?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="OpenShift AI - Creating a Workbench" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "OpenShift AI - Elyria Pipeline Editor",
          description: "Explore the Elyria Pipeline Editor in OpenShift AI.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/wf6QCSa3fs7IkUJ6RsK3?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="OpenShift AI - Elyria Pipeline Editor" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "OpenShift AI Pipelines",
          description: "Learn about pipelines in OpenShift AI.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Agr7Q4ziozdySTA7KaWF?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="OpenShift AI Pipelines" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "RHEL AI - Intro",
          description: "An introduction to RHEL AI.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/tCzxg3N2kdcn7KFLXLk1?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "RHEL-AI-Doc Processing with Docling",
          description: "Process documents with RHEL AI and Docling.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Ktu3ADY3aWDOlgYxKILH?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL-AI-Doc Processing with Docling" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "How Red Hat can help with AI adoption",
          description: "Discover how Red Hat supports AI adoption.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/cgYsaBPDxWsGtbtf9bke?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="How Red Hat can help with AI adoption" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "Training a model in Red Hat OpenShift AI",
          description: "Learn how to train models in Red Hat OpenShift AI.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/nIIl1xawNscq0wO5Pd0i?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Training a model in Red Hat OpenShift AI" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "Prepare and label custom datasets with Label Studio on Red Hat OpenShift",
          description: "Prepare and label custom datasets using Label Studio on Red Hat OpenShift.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/3zkaOQGEMciCrOD9zGys?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Prepare and label custom datasets with Label Studio on Red Hat OpenShift" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        

      ],
    },

    {
      section: "Virtualization",
      demos: [
        {
          title: "Scale VM migration to OpenShift with AAP",
          description: "Streamline virtual machine migration to OpenShift with Ansible.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/nOMqJgJKjLnHrJTNzwne?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Manage and monitor VMs on OpenShift with ACM",
          description: "Effortlessly manage and monitor VMs with Red Hat ACM.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/PIbyWf2YD2A6BqwXp6hz?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Migrate VMs from VMware to Red Hat OpenShift",
          description: "Simplify VMware VM migration to OpenShift with efficient tools.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Y9IbIXh5kGrjW9Q3v7Tf?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Data protection for OpenShift Virtualization with Trilio",
          description: "Ensure secure VM backups and recovery with Trilio on OpenShift.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/lNxNbYE3xpoW52wJsZY7?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Live Migration for VMs on OpenShift Virtualization",
          description: "Migrate VMs live on OpenShift without service interruptions.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/cOpVnpCc5KHB3oxt72GT?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Cloning VMs on OpenShift Virtualization",
          description: "Easily clone virtual machines for OpenShift with advanced tools.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Ao7y9uv5Ot9scXPMlLXm?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Some reasons to migrate to Red Hat OpenShift Virtualization",
          description: "Explore key benefits of migrating to OpenShift Virtualization today.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/UF2n3mlFS6x10ggD5VvH?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Regional Disaster Recovery for VMs using RHACM and ODF",
          description: "Implement regional disaster recovery for VMs with RHACM and ODF.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/DaVCtST5FAksCu7w0vZp?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Self-service provisioning VMs with Red Hat Developer Hub (Platform Engineering)",
          description: "Provision virtual machines using Red Hat Developer Hub.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Dtucjw9jV2mRAGFAi420?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Self-service provisioning VMs with Red Hat Developer Hub (Platform Engineering)" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
      ],
    },



      {
        section: "Application Platform",
        demos: [
          {
            title: "Signing code in Red Hat Trusted Application Pipeline",
            description: "Securely sign code using Red Hat Trusted Application Pipeline.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Mki2lmgR4O9KLypj9woc?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Signing code in Red Hat Trusted Application Pipeline" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          
          {
            title: "Deploy Red Hat Developer Hub on OpenShift Developer Sandbox",
            description: "Step-by-step guide to deploying Red Hat Developer Hub on OpenShift Developer Sandbox.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/9MdJTUKpj0hY8CsOei8G?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Deploy Red Hat Developer Hub on OpenShift Developer Sandbox" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Onboarding made easy with Red Hat Developer Hub",
            description: "Simplify onboarding processes with Red Hat Developer Hub.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/CM9E9X8Ejh7EkGvru6Bm?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Onboarding made easy with Red Hat Developer Hub" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Extend and Enable Application Capabilities to end-users",
            description: "Discover how to extend and enable application capabilities for end-users.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/gbTwA2hmvmHmul1Ri98O?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Extend and Enable Application Capabilities to end-users" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Self-service provisioning environments with Red Hat Developer Hub",
            description: "Enable self-service provisioning environments with Red Hat Developer Hub.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/tJrKG4RgoyAUdat2K23R?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Self-service provisioning environments with Red Hat Developer Hub" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Introduction to Self-service provisioning with Red Hat Developer Hub",
            description: "An introduction to self-service provisioning with Red Hat Developer Hub.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/agpRgd9usObs4XJ5mytw?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Introduction to Self-service provisioning with Red Hat Developer Hub" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Red Hat Connectivity Link - Platform Engineer",
            description: "Explore Red Hat Connectivity Link for Platform Engineers.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/NsFCqBGTiYftTz9rAAN0?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Red Hat Connectivity Link - Platform Engineer" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Red Hat Connectivity Link - Developer",
            description: "Explore developer connectivity links with Red Hat.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/YQqEIctPp7NPKgjgieJz?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Red Hat Connectivity Link - Developer" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Event Streaming And Real Time Data Processing",
            description: "Learn about event streaming and real-time data processing.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/ckRFznUyuMykk7IeTulQ?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Event Streaming And Real Time Data Processing" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Self-service provisioning VMs with Red Hat Developer Hub (Platform Engineering)",
            description: "Provision virtual machines using Red Hat Developer Hub.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Dtucjw9jV2mRAGFAi420?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Self-service provisioning VMs with Red Hat Developer Hub (Platform Engineering)" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          
          
        ],
    },

    {
      section: "Mission Critical Automation",
      demos: [
        {
          title: "Scale VM migration to OpenShift with AAP",
          description: "Migrate VMs to OpenShift efficiently using Ansible Automation Platform.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/nOMqJgJKjLnHrJTNzwne?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "Event-Driven Ansible with OpenShift",
          description: "Automate OpenShift workflows with event-driven Ansible for efficiency.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/0Ecs1yfeHQDJIZsD9aUV?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        {
          title: "S/4 HANA Deployment with Ansible Automation Platform",
          description: "Learn how to deploy S/4 HANA using Ansible Automation Platform.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/8iuEXvRPoR4BBr5zVyg5?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="S/4 HANA Deployment with Ansible Automation Platform" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "Configure Ansible-S/4 HANA Deployment in Ansible Automation Platform",
          description: "Configure and manage S/4 HANA deployments using Ansible Automation Platform.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/VwJyLb3dQ9gs6uGoKsUI?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Configure Ansible-S/4 HANA Deployment in Ansible Automation Platform" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        
        {
          title: "Ansible Automation Platform on Microsoft Azure - Managed Application Demo",
          description: "Learn how to manage applications with Ansible Automation Platform on Microsoft Azure.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/liRbTzSPhtuQZwTI3JSV?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Ansible Automation Platform on Microsoft Azure - Managed Application Demo" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "Red Hat Ansible Automation Platform Service on AWS Demo",
          description: "Discover the features of Red Hat Ansible Automation Platform Service on AWS.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Xe9tI8rUVc2JrEI5LprL?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Red Hat Ansible Automation Platform Service on AWS Demo" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        {
          title: "Ansible Lightspeed 90-day Trial Demo",
          description: "Get a hands-on preview of Ansible Lightspeed with this 90-day trial demo.",
          embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/ZiJZMunDn0fg4YUHy2Br?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Ansible Lightspeed 90-day Trial Demo" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
        },
        
        
        




      ],
  },

    {
        section: "Container Management",
        demos: [
          {
            title: "Single Node OpenShift deployment with the Assisted Installer tool",
            description: "Deploy Single Node OpenShift using the Assisted Installer tool.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/zPclCmIO6SL2qW1mveEj?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Single Node OpenShift deployment with the Assisted Installer tool" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "GPU Enablement on Single Node OpenShift",
            description: "Enable GPU capabilities on Single Node OpenShift deployments.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/t95FlUL0InmvrRWjF0Pf?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="GPU Enablement on Single Node OpenShift" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Network security with Red Hat Advanced Cluster Security for Kubernetes",
            description: "Enhance network security using Red Hat Advanced Cluster Security for Kubernetes.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/E8hrBwTp78IQinG2e3ck?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Network security with Red Hat Advanced Cluster Security for Kubernetes" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          {
            title: "Vulnerability and risk management with ACS",
            description: "Manage vulnerabilities and risks with Red Hat Advanced Cluster Security.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/O6vrNXUt4mYM9A7NVFLm?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Vulnerability and risk management with ACS" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },

          
      
          
          {
            title: "Red Hat OpenShift Service on AWS (ROSA)",
            description: "Learn how to leverage Red Hat OpenShift Service on AWS (ROSA) for deploying and managing applications.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/Moqqu1ZBptppDgh8cooz?embed&embed_mobile=tab&embed_desktop=tab&show_copy_link=true" title="Red Hat OpenShift Service on AWS (ROSA)" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          
          
          
          {
            title: "Ephemeral environment provisioning with Developer Hub and ACM",
            description: "Provision ephemeral environments using Red Hat Developer Hub and ACM.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/DpgaHIMOFVnyZS31tGoD?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Ephemeral environment provisioning with Developer Hub and ACM" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },

          {
            title: "Policy management with Red Hat Advanced Cluster Security for Kubernetes",
            description: "Manage policies effectively using Red Hat Advanced Cluster Security for Kubernetes.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/8qwgdKkhEirhTu7ubCWg?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="Policy management with Red Hat Advanced Cluster Security for Kubernetes" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          

          
          
          
          
          
        ],
      },



    {
        section: "Server/Cloud Operating System",
        demos: [
          {
            title: "How to build a RHEL 10 Beta image for AWS with Insights Image Builder",
            description: "Learn to create RHEL 10 Beta images for AWS deployment.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/BOkVngmzx3CyHKlAYepn?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },
          {
            title: "How image mode for RHEL can help with upgrades",
            description: "Discover how image mode simplifies RHEL upgrades effectively.",
            embedCode: `<div style="position: relative; padding-bottom: calc(64.64120370370371% + 41px); height: 0; width: 100%;"><iframe src="https://demo.arcade.software/ai58IaAzCqWNfneJM26E?embed&embed_mobile=tab&embed_desktop=inline&show_copy_link=true" title="RHEL AI - Intro" frameborder="0" loading="lazy" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="clipboard-write" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; color-scheme: light;"></iframe></div>`,
          },

        ],
    },



  
  ];
  export const introContent = {
    title: "Interactive Experiences",
    description:
      "Explore interactive experiences designed to showcase Red Hat's solutions, from AI platforms to application platforms. Start learning through hands-on experiences.",
  };


  export const sidebarLinks = [
    { id: "ai", text: "AI Platform" },
    { id: "virtualization", text: "Virtualization" },
    { id: "app", text: "Application Platform" },
    { id: "automation", text: "Mission Critical Automation" },
    { id: "container", text: "Container Management" },
    { id: "os", text: "Server/Cloud Operating System" },
    
  ];
  