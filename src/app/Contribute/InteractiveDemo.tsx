import React, { useState } from "react";
import { Helmet } from "react-helmet";
import {
  Page,
  PageSection,
  Grid,
  GridItem,
  Card,
  CardBody,
  TextInput,
} from "@patternfly/react-core";
import { introContent, sidebarLinks, demoContent } from "./content";

const InteractiveDemo = () => {
  const [searchTerm, setSearchTerm] = useState("");

  const handleSearchChange = (value) => setSearchTerm(value.toLowerCase());

  // Process sections based on search term
  const processedSections = demoContent.map((section) => {
    const filteredDemos = section.demos.filter((demo) =>
      demo.title.toLowerCase().includes(searchTerm)
    );
    return {
      ...section,
      filteredDemos,
      resultCount: filteredDemos.length,
    };
  });

  // Sort sections by result count only when there is a search term
  const sortedSections = searchTerm
    ? processedSections.sort((a, b) => b.resultCount - a.resultCount)
    : processedSections;

  return (
    <Page id="learnmore">
      {/* SEO Meta Tags */}
      <Helmet>
        <title>Red Hat Architecture Center - Interactive Experiences Page</title>
        <meta
          name="description"
          content="Explore guided demos for Red Hat solutions with interactive video walkthroughs."
        />
        <meta
          name="keywords"
          content="Red Hat, Interactive Experiences, Architecture Center"
        />
      </Helmet>

      {/* Intro Section */}
      <PageSection variant="light" className="intro-section full-width">
        <Grid hasGutter>
          <GridItem lg={7} md={6} sm={12} className="intro-text-column">
            <h1 className="intro-title">{introContent.title}</h1>
            <p className="intro-description">{introContent.description}</p>
          </GridItem>
          <GridItem lg={1} md={1} sm={12} />
          <GridItem lg={4} md={5} sm={12} className="featured-card-column">
            <Card className="featured-card">
              <CardBody>
                <p className="featured-card-label">Featured</p>
                <div
                  dangerouslySetInnerHTML={{
                    __html: demoContent[0]?.demos[0]?.embedCode || "",
                  }}
                />
                <p className="featured-card-description">
                  {demoContent[0]?.demos[0]?.description || "No Description"}
                </p>

              </CardBody>
            </Card>
          </GridItem>
        </Grid>
      </PageSection>

      {/* Page Content */}
      <div
        className="page-content"
        style={{ width: "100%", maxWidth: "1600px", margin: "0 auto" }}
      >
        <Grid hasGutter>
          {/* Sidebar */}
          <GridItem lg={3} md={4} sm={12}>
            <PageSection id="toc-menu">
              <div className="toc" id="toc">
                <h3>Solutions</h3>
                <ul>
                  {sidebarLinks.map((link, index) => (
                    <li key={index}>
                      <a href={`portfolio/interactive-experiences#${link.id}`}>
                        {link.text}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </PageSection>
          </GridItem>

          {/* Main Content */}
          <GridItem lg={9} md={8} sm={12}>
            <PageSection>
              <TextInput
                value={searchTerm}
                type="text"
                onChange={(value) => handleSearchChange(value)}
                aria-label="Search interactive experiences"
                placeholder="Search interactive experiences..."
              />
            </PageSection>

            {/* Render Sections Dynamically */}
            {sortedSections.map((section) => (
  <PageSection
    key={section.section}
    id={sidebarLinks.find((link) => link.text === section.section)?.id}
  >
    <h2>
      {section.section}{"  "}
      <span style={{ fontSize: "0.7em", marginLeft: "8px" }}>
        ({section.resultCount})
      </span>
    </h2>
    <Grid hasGutter>
      {section.filteredDemos.map((demo, demoIndex) => (
        <GridItem key={demoIndex} lg={3} md={6} sm={12}>
          <Card className="catalog_card_experience_b" style={{ padding: "10px" }}>
            <CardBody style={{ padding: "5px" }}>
              {/* Hidden Title (accessible for search) */}
              <div style={{ display: "none" }}>{demo.title}</div>
              {/* Embed Iframe at the Top */}
              <div
                className="embed_container"
                dangerouslySetInnerHTML={{
                  __html: demo.embedCode,
                }}
              />
              {/* Description */}
              <p style={{ fontSize: "0.85rem", marginTop: "10px" }}>
                {demo.description}
              </p>
            </CardBody>
          </Card>
        </GridItem>
      ))}
    </Grid>
    {section.resultCount === 0 && (
      <p style={{ textAlign: "center", color: "gray" }}>
        No results found for this section.
      </p>
    )}
  </PageSection>
))}

          </GridItem>
        </Grid>
      </div>
    </Page>
  );
};

export default InteractiveDemo;
