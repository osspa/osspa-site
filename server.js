const express = require('express');
const app = express();

const webpack = require('webpack');
const path = require('path');
const middleware = require('webpack-dev-middleware');
const config = require('./webpack.prod.js');
const compiler = webpack(config);
const history = require('connect-history-api-fallback');
const { createProxyMiddleware } = require("http-proxy-middleware");
const fs = require('fs');
const os = require('os');

let PORT = 8000;

// Serve static files
app.use(express.static(__dirname + '/asset'));

// Error logging middleware for capturing server errors
app.use((err, req, res, next) => {
  console.error(`Error occurred on ${req.method} ${req.url}:`, err);
  res.status(500).send('Internal Server Error');
});

// History API Fallback middleware for single-page apps
app.use(history());

// Environment-based serving logic
if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "dist/architect/portfolio")));
} else {
  PORT = 8081;
  app.use(middleware(compiler, {
    // Webpack-dev-middleware options
  }));
}

// API route
app.get('/api', (req, res) => {
  res.send('<p>This is an API Data</p>');
});

// Proxy setup for GitLab and other external services
app.use(
  "/osspa",
  createProxyMiddleware({
    target: "https://gitlab.com/",
    changeOrigin: true
  })
);

app.use(
  "/architect/portfolio/tool/gitlab.com/osspa",
  createProxyMiddleware({
    target: "https://gitlab.com/",
    changeOrigin: true,
    pathRewrite: {
      '^/architect/portfolio/tool/gitlab.com/osspa': 'https://gitlab.com/osspa'
    }
  })
);

app.use(
  "/architect/portfolio/osspa",
  createProxyMiddleware({
    target: "https://gitlab.com/",
    changeOrigin: true,
    pathRewrite: {
      '^/architect/portfolio/osspa': 'https://gitlab.com/osspa'
    }
  })
);

// Reverse proxy for Solution Patterns
app.use(
  "/architect/portfolio/redhat-solution-patterns",
  createProxyMiddleware({
    target: "https://raw.githubusercontent.com/",
    changeOrigin: true,
    pathRewrite: {
      '^/architect/portfolio/redhat-solution-patterns': 'https://raw.githubusercontent.com/redhat-solution-patterns'
    }
  })
);

// Reverse proxy for Validated Patterns
app.use(
  "/architect/portfolio/hybrid-cloud-patterns",
  createProxyMiddleware({
    target: "https://raw.githubusercontent.com/",
    changeOrigin: true,
    pathRewrite: {
      '^/architect/portfolio/hybrid-cloud-patterns': 'https://raw.githubusercontent.com/hybrid-cloud-patterns'
    }
  })
);

// 404 handler for undefined routes
app.use((req, res) => {
  console.log(`404 Not Found: ${req.method} ${req.url}`);
  res.status(404).send('Not Found');
});

// Start the server
app.listen(PORT, () => {
  console.log(`Portfolio Public Site listening on port ${PORT}`);
});

// Dynamically generate template XML tools
const os_hostname = os.hostname();
let HOSTNAME = 'www.redhat.com';

if (os_hostname === 'portfolio-wnix-us' && PORT === 8081) {
  HOSTNAME = 'dev.osspa.org';
} else if (os_hostname === 'portfolio-wnix-us') {
  HOSTNAME = 'osspa.org';
}

const { create } = require('xmlbuilder2');

const xml_root = create({ version: '1.0' })
  .ele('templates')
  .ele('template', { section: 'Red Hat Templates', url: `https://${HOSTNAME}/architect/portfolio/tool/Templates/DetailDiagram.xml`, title: 'Detail Diagram', preview: `https://${HOSTNAME}/architect/portfolio/tool/Templates/DetailDiagram.png`, libs: '' })
  .up()
  .ele('template', { section: 'Red Hat Templates', url: `https://${HOSTNAME}/architect/portfolio/tool/Templates/LogicalDiagram.xml`, title: 'Logical Diagram', preview: `https://${HOSTNAME}/architect/portfolio/tool/Templates/LogicalDiagram.png`, libs: '' })
  .up()
  .ele('template', { section: 'Red Hat Templates', url: `https://${HOSTNAME}/architect/portfolio/tool/Templates/SchematicDiagram.xml`, title: 'Schematic Diagram', preview: `https://${HOSTNAME}/architect/portfolio/tool/Templates/SchematicDiagram.png`, libs: '' })
  .up()
  .ele('template', { section: 'Red Hat Templates', url: `https://${HOSTNAME}/architect/portfolio/tool/Templates/RedHatAllAssets.xml`, title: 'All Diagrams', preview: `https://${HOSTNAME}/architect/portfolio/tool/Templates/RedHatAllAssets.png`, libs: '' })
  .up();

// Write the XML to a file
const xml = xml_root.end({ prettyPrint: true });
const full_file_name = "./asset/architect/portfolio/tool/template.xml";
fs.writeFileSync(full_file_name, xml, (err) => {
  if (err) throw err;
});

